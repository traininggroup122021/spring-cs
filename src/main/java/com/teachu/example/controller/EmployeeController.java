package com.teachu.example.controller;

import java.util.Date;
import java.util.List;

import com.teachu.example.model.Employee;
import com.teachu.example.repository.EmployeeRepository;
import org.bson.types.ObjectId;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.web.bind.annotation.*;

class EmployeeNotFoundException extends RuntimeException {

    EmployeeNotFoundException(ObjectId id) {
        super("Could not find employee " + id);
    }
}

@RestController
@RequestMapping(value = "/")
class EmployeeController {

    private final EmployeeRepository repository;

    EmployeeController(EmployeeRepository repository) {
        this.repository = repository;
    }


    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/employees")
    List<Employee> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]

    @PostMapping("/employees")
    Employee newEmployee(@RequestBody Employee newEmployee) {
        return repository.save(newEmployee);
    }

    @GetMapping("/employees/addSome")
    void addSome() {
        // Save the employees
        repository.save(new Employee("Atul", "Rai", 'M', 28));
        repository.save(new Employee("Pallavi", "Tripathi", 'F', 25));
        repository.save(new Employee("Manish", "Fartiyal", 'M', 30));
        repository.save(new Employee("Lucifer", "**", 'F', 33));
    }

    // Single item

    @GetMapping("/employees/{id}")
    Employee one(@PathVariable ObjectId id) {

        return repository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException(id));
    }

    @PutMapping("/employees/{id}")
    Employee replaceEmployee(@RequestBody Employee newEmployee, @PathVariable ObjectId id) {

        return repository.findById(id)
                .map(employee -> {
                    employee.setFirstName(newEmployee.getFirstName());
                    employee.setLastName(newEmployee.getLastName());
                    employee.setGender(newEmployee.getGender());
                    employee.setAge(newEmployee.getAge());
                    return repository.save(employee);
                })
                .orElseGet(() -> {
                    newEmployee.set_id(id);
                    return repository.save(newEmployee);
                });
    }

    @DeleteMapping("/employees/{id}")
    void deleteEmployee(@PathVariable ObjectId id) {
        repository.deleteById(id);
    }
}