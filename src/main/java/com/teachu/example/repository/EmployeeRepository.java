package com.teachu.example.repository;

import com.teachu.example.model.Employee;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, ObjectId> {

    List<Employee> findByFirstName(String firstName);
    List<Employee> findByGender(Character gender);
    Optional<Employee> findById(ObjectId id);
    List<Employee> findAll();
}