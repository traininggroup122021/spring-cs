# Definitions
## starting

- `docker-compose up -d` to startup the services
- `docker exec -it <container-name> bash` to start interactive shell in the terminal

### in the mongo container
`mongo -u <your username> -p <your password> --authenticationDatabase <your database name>`
OR
`mongo -u <your username> --authenticationDatabase <your database name>`

### Reachable
Url for the container is: `mongodb://YourUsername:YourPasswordHere@127.0.0.1:27017/your-database-name`

## init-mongo.js
this is the script which should setup some data in the mongodb on startup

## docker-compose
- `image` must be mongo, because you want to create a container from mongo image
- `container_name` is a name for your container, it’s optional
- `environment` is a variables that will be used on the mongo container
- `MONGO_INITDB_DATABASE` you fill with a database name that you want to create, make it same like init-mongo.js
- `MONGO_INITDB_ROOT_USERNAME` you fill with username of root that you want
- `MONGO_INITDB_ROOT_PASSWORD` you fill with password of root that you want
- `volumes` to define a file/folder that you want to use for the container
- `./init-mongo.js:/docker-entrypoint-initdb.d/init-mongo-js:ro` means you want to copy init-mongo.js to /docker-entrypoint-initdb.d/ as a read only file. /docker-entrypoint-initdb.d is a folder that already created inside the mongo container used for initiating database, so we copy our script to that folder
- `./mongo-volume:/data/db` means you want to set data on container persist on your local folder named mongo-volume . /data/db/ is a folder that already created inside the mongo container.
- `ports` is to define which ports you want to expose and define, in this case I use default mongoDB port 27017 until 27019