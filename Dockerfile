FROM maven:3.8.4-openjdk-17 as BUILD

EXPOSE 8080

COPY ./target/spring-boot-example-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app

ENTRYPOINT ["java", "-jar", "spring-boot-example-0.0.1-SNAPSHOT.jar"]


